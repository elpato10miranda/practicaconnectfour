package connectfour_protocolo;

/**
 *
 * @author Matthew Miranda
 */
public class ConnectFour_protocolo {

    public static final int PUERTO_POR_DEFECTO = 1234;        
    
    public static final String CMD_FINALIZACION = "-STOP";
    
    public static final String CMD_IDVENTANA = "getID";
    
}
