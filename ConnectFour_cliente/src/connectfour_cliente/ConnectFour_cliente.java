package connectfour_cliente;

import connectfour_cliente.control.ControlCF;
import connectfour_cliente.vista.VentanaAplicacion;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.swing.SwingUtilities;

/**
 *
 * @author Matthew Miranda
 */
public class ConnectFour_cliente {

    /**
     * @param args the command line arguments
     */
    private static Cliente c;
    private static final String DIRECCION_SERVIDOR = "localhost";
    private static int cantJugadores;

    public static void main(String[] args) throws IOException {
        String servidor = DIRECCION_SERVIDOR;
        if (args.length > 0) {
            servidor = args[0];
            System.out.printf("Servidor ubicado en la direccion: '%s'%n", servidor);
        }
        
        iniciarJuego();    
    }

    public static void iniciarJuego() {
        SwingUtilities.invokeLater(() -> {
            new VentanaAplicacion("CONNECT FOUR", new ControlCF(new Cliente())).inicializarVentana();
        });
    }

    public static int solicitarCantidadJugadores() throws IOException {
        int cantJugadores = 0;
        System.out.println("INGRESE LA CANTIDAD DE JUGADORES QUE DESEAN JUGAR :");
        System.out.print("-> ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String cantidad = reader.readLine();
        cantJugadores = Integer.parseInt(cantidad);
        return cantJugadores;
    }

}
