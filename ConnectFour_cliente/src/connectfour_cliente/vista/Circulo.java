/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connectfour_cliente.vista;

import java.awt.Color;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;

/**
 *
 * @author Kenneth Sibaja
 */
public class Circulo {
    
    private Color color;    
    private int x, y;
    private int fila, columna;
    private Shape circulo;

    public Circulo(Color color, int x, int y, int fila, int columna) {
        this.color = color;
        this.x = x;
        this.y = y;
        this.fila = fila;
        this.columna = columna;
        
        circulo = new Ellipse2D.Double(x, y, 70, 70);
    }   
    
    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Shape getCirculo() {
        return circulo;
    }

    public void setCirculo(Shape circulo) {
        this.circulo = circulo;
    }

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getColumna() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }
    
    
    
    
}
