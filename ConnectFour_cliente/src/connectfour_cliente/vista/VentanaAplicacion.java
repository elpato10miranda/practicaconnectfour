package connectfour_cliente.vista;

import connectfour_cliente.control.ControlCF;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Matthew Miranda
 */
public class VentanaAplicacion extends JFrame implements Observer {

    private final ControlCF gestorPrincipal;
    private Tablero tablero;

    public VentanaAplicacion(String titulo, ControlCF nuevoGestor) {
        super(titulo);
        this.gestorPrincipal = nuevoGestor;        
        configurar();        
    }

    @Override
    public void update(Observable o, Object arg) {
        //System.out.println("Actualizando..");
    }

    private void configurar() {
        ajustarComponentes(getContentPane());
        setResizable(true);
        setSize(850, 710);
        setMinimumSize(new Dimension(850, 710));
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private void ajustarComponentes(Container c) {
        c.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.NORTHWEST;
        
        gbc.fill = GridBagConstraints.BOTH;
        
        gbc.weightx = 0.1;
        gbc.weighty = 0.1;
        
        c.add(tablero = new Tablero(gestorPrincipal), gbc);
    }

    public void inicializarVentana() {
//        gestorPrincipal.registrar(this);
        setVisible(true);
    }
    
    @Override
    public String toString() {
        return "Ventana: " + super.getTitle();
    }

    public VentanaAplicacion() throws HeadlessException {
        this.gestorPrincipal = null;
    }

}
