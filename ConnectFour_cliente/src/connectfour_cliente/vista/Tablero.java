/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connectfour_cliente.vista;

import connectfour_cliente.control.ControlCF;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;

/**
 *
 * @author Kenneth Sibaja
 */
public class Tablero extends JPanel {

    private ControlCF gestorPrincipal;
    private List<Circulo> listaCirculos;

    public Tablero(ControlCF gp) {
        listaCirculos = new ArrayList<>();
        gestorPrincipal = gp;
        configurar();
    }

    public void configurar() {
        setBackground(Color.YELLOW.darker());
        cargarLista();
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                comprobarClick(e.getPoint());
            }
            
        });
    }

    public void comprobarClick(Point p) {
        for (Circulo c : listaCirculos) {
            if (c.getCirculo().contains(p)) {
                gestorPrincipal.enviarClick(c);
            }
        }
    }    

    private void cargarLista() {
        int x = 105, y = 20;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                listaCirculos.add(new Circulo(Color.WHITE, x, y, i, j));
                x += 80;
            }
            x = 105;
            y += 80;
        }
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        int x = 0;
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(2));
        for (Circulo c : listaCirculos) {
            x = c.getX();
            g2.setColor(c.getColor());
            g2.fill(c.getCirculo());
            g2.setColor(Color.BLACK);
            g2.drawLine(x - 4, 15, x - 4, 655);
        }
        g2.setColor(Color.BLACK);
        g2.drawLine(x + 76, 15, x + 76, 655);
        g2.drawLine(100, 14, 741, 14);
        g2.drawLine(100, 656, 741, 656);
    }

}
