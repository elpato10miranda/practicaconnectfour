package connectfour_cliente.control;

import connectfour_cliente.Cliente;
import connectfour_cliente.vista.Circulo;
import connectfour_servidor.modelo.ModeloCF;
import java.util.HashMap;
import java.util.Observer;

/**
 *
 * @author Matthew Miranda
 */
public class ControlCF {
    
    private Cliente cliente;
    private int idJugador;

    public ControlCF(Cliente cliente) {
        this.cliente = cliente;
        idJugador = cliente.getIDCliente();
    }        
    
    
    public void enviarClick(Circulo c){
        HashMap map = new HashMap();
        map.put("fila", c.getFila());
        map.put("columna", c.getColumna());                
        map.put("jugador", idJugador);
        cliente.solicitarServicios(map);
    }
    
    
    
}
