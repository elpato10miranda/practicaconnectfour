package connectfour_cliente;

import connectfour_protocolo.ConnectFour_protocolo;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author Matthew Miranda
 */
public class Cliente {

    private int idCliente;
    private int puerto;
    private Socket skt;
    ObjectOutputStream salida;
    ObjectInputStream entrada;

    public Cliente() {
        iniciar("localhost");        
    }

    public void iniciar(String servidor) {
        try {
            puerto = ConnectFour_protocolo.PUERTO_POR_DEFECTO;
            System.out.printf("Intentando conectarse en: %s:%s%n", servidor, puerto);
            try (Socket skt = new Socket(servidor, puerto)) {
                System.out.println("Conexión realizada..");
                this.skt = skt;
                salida = new ObjectOutputStream(skt.getOutputStream());
                entrada = new ObjectInputStream(skt.getInputStream());
                System.out.println("Stand-by");
                obtenerIdCliente();
            }
        } catch (IOException e) {
            System.err.printf("%s%n", e.getMessage());
        }
    }

    public void obtenerIdCliente() {
        try {

            System.out.println("Solicitando id de la ventana..");

            boolean finalizado = false;

            String mensaje = ConnectFour_protocolo.CMD_IDVENTANA, idRecibido;
            salida.writeObject(mensaje);
            salida.flush();

            while (!finalizado) {

                Object obj = entrada.readObject();
                if (obj != null) {
                    idRecibido = (String) obj;
                    System.out.println("id Solicitado");
                    System.out.println(idRecibido);
                    idCliente = Integer.valueOf(idRecibido);
                    finalizado = true;
                }

            }

        } catch (Exception e) {
        }
    }

    public void solicitarServicios(HashMap p) {
        try{

            salida.writeObject(p);
            salida.flush();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getIDCliente() {
        return idCliente;
    }
}
