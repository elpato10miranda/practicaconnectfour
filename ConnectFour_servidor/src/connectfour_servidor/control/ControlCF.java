/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connectfour_servidor.control;

import connectfour_servidor.modelo.ModeloCF;
import java.util.HashMap;

/**
 *
 * @author Kenneth Sibaja
 */
public class ControlCF {
    
    private ModeloCF datos;

    public ControlCF() {
        this.datos = new ModeloCF();
    }
    
    public HashMap obtenerColorCelda(int fila, int columna){
        return datos.obtenerColorCelda(fila, columna);
    }
    
    public HashMap cambioColor(int fila, int columna, int jugador){
        return datos.cambioColor(fila, columna, jugador);
    }
    
}
