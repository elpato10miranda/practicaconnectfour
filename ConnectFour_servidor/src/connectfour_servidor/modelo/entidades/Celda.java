/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connectfour_servidor.modelo.entidades;



/**
 *
 * @author Kenneth Sibaja
 */
public class Celda {

    private int fila;
    private int columna;
    private int ocupado;
    private int colorActual;

    public Celda(int fila, int columna, int ocupado) {
        this.fila = fila;
        this.columna = columna;
        this.ocupado = ocupado;
        this.colorActual = 0;
    }
    
    public void cambiarColor(int jug){
        colorActual = jug;
    }

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getColumna() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }

    public int getOcupado() {
        return ocupado;
    }

    public void setOcupado(int ocupado) {
        this.ocupado = ocupado;
    }

    public int getColorActual() {
        return colorActual;
    }

    public void setColorActual(int colorActual) {
        this.colorActual = colorActual;
    }    
    
}
