package connectfour_servidor.modelo;

import connectfour_servidor.modelo.entidades.Celda;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;

/**
 *
 * @author Matthew Miranda
 */
public class ModeloCF extends Observable {
    
    private List<Celda> listaCeldas;
    
    public ModeloCF() {
        System.out.println("Inicializando modelo..");
        listaCeldas = new ArrayList<>();        
        inicializarCeldas();
    }
    
    public void inicializarCeldas(){
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                listaCeldas.add(new Celda(i, j, 0));
            }
        }
    }
    
    public HashMap cambioColor(int fila, int columna, int jugador){
        HashMap celda = new HashMap();
        for(Celda c : listaCeldas){
            if(c.getFila() == fila && c.getColumna() == columna){
                if(c.getOcupado() == 0){
                    c.cambiarColor(jugador);
                    celda.put("fila", c.getFila());
                    celda.put("columna", c.getColumna());
                    celda.put("color", c.getColorActual());
                }
            }
        }
        return celda;
    }
    
    public HashMap obtenerColorCelda(int fila, int columna){
        HashMap celda = new HashMap();
        for(Celda c : listaCeldas){
            if(c.getFila() == fila && c.getColumna() == columna){
                celda.put("color", c.getColorActual());                
            }
        }
        return celda;
    }
    
    private void actualizarDatos() {
        setChanged();
    }
    
    public void clickCirculo(/*AreaParaCirculos apc*/){
        System.out.println("Click en una casilla");
        actualizarDatos();
        notifyObservers();
    }

}
