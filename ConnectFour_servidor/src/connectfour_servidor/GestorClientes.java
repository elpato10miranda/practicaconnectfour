package connectfour_servidor;

import connectfour_protocolo.ConnectFour_protocolo;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;

/**
 *
 * @author Matthew Miranda
 */
public class GestorClientes {

    private static int instancias = 0;
    private final int id;
    private final Socket skt;

    public GestorClientes(Socket skt) {
        this.id = ++instancias;
        this.skt = skt;
    }
    
    public void atenderSolicitudes(){
        new Thread(() -> {
            
            boolean finalizado = false;            
            Object obj = null;
            try{
                try(ObjectOutputStream salida = new ObjectOutputStream(skt.getOutputStream());
                ObjectInputStream entrada = new ObjectInputStream(skt.getInputStream())) {
                                                         
                    while(!finalizado){                                             
                        try {
                            obj = entrada.readObject();
                        } catch (IOException | ClassNotFoundException iOException) {
                            obj = null;
                        }
                        if (obj != null) {
                            if (obj.getClass().equals(String.class)) {                                
                                if (obj.equals(ConnectFour_protocolo.CMD_IDVENTANA)) {
                                    System.out.println("Retornando id ventana");
                                    salida.writeObject(String.valueOf(id));
                                    salida.flush();
                                    System.out.println("Stand-By..");
                                }
                                if (obj.equals(ConnectFour_protocolo.CMD_FINALIZACION)) {
                                    finalizado = true;
                                }
                            } else if (obj.getClass().equals(HashMap.class)) {
                                System.out.println("HashMap recibido");
                            }
                        }                                           
                    }
                    
                } 
                
            } catch(Exception ex){
                System.out.println("Caio en el catch GESTOR CLIENTES");
                ex.printStackTrace();
            }
            
            try {
                skt.close();
            } catch (Exception e) {
               
            }
            
        }).start();
    }

}
