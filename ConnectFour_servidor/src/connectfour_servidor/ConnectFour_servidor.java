package connectfour_servidor;

import java.io.IOException;

/**
 *
 * @author Matthew Miranda
 */
public class ConnectFour_servidor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {        
        new Servidor().iniciarServidor();
    }

}
