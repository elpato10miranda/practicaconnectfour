/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connectfour_servidor;

import connectfour_protocolo.ConnectFour_protocolo;
import connectfour_servidor.control.ControlCF;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author Matthew Miranda
 */
public class Servidor {

    private static final int MAX_ESPERA = 50000;
    private ControlCF gestorPrincipal;

    public Servidor() {
        gestorPrincipal = new ControlCF();
    }

    public void iniciarServidor() throws IOException {
        try {
            int puerto = ConnectFour_protocolo.PUERTO_POR_DEFECTO;
            System.out.printf("Abriendo socket de servidor en: %d%n", puerto);
            ServerSocket srv = new ServerSocket(puerto);
            new Thread(() -> {

                boolean activo = true;
                try {
                    System.out.printf("Esperando conexión en: %s:%d%n",
                            InetAddress.getLocalHost().getHostAddress(), puerto);
                } catch (UnknownHostException ex0) {
                    System.err.printf("Excepción: '%s'%n", ex0.getMessage());
                    activo = false;
                }
                while (activo) {
                    try {
                        // El servidor espera la conexión de un cliente..

                        srv.setSoTimeout(MAX_ESPERA);
                        Socket skt = srv.accept();
                        System.out.printf("Conexión completada: %s%n",
                                skt.getRemoteSocketAddress());
                        GestorClientes cliente = new GestorClientes(skt);

                        // El cliente es el responsable de cerrar el socket
                        // cuando termine la ejecución.
                        cliente.atenderSolicitudes();
                    } catch (InterruptedIOException e) {
                        e.printStackTrace();
                        break;
                    } catch (IOException e1) {
                        System.err.printf("Excepción: '%s'%n", e1.getMessage());
                    }
                }

            }).start();
        } catch (IOException e2) {
            System.err.printf("Excepción: '%s'%n", e2.getMessage());
        }
    }
}
